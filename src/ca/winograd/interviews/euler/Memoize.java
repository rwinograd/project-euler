package ca.winograd.interviews.euler;

import java.util.HashMap;

public class Memoize {
	
	// TODO: Properly handle recursive memoization
	public static <Input, Output> Function<Input, Output> memoize(final Function<Input, Output> fn) {
		return new Function<Input, Output>() {
			final HashMap<Input, Output> map = new HashMap<Input, Output>();
			@Override
			public Output apply(Input input) {
				if(!map.containsKey(input)) {
					map.put(input, fn.apply(input));
				}
				return map.get(input);
			}
			
		};
	}
	
	public static <Input, Output> Function<Input, Output> memoize(final RecursiveFunction<Input, Output> trueRecursive) {
		final Function<Input, Output> [] entryPoint = new Function[1];
		
		entryPoint[0] = new Function<Input, Output>() {
			final HashMap<Input, Output> map = new HashMap<Input, Output>();
			@Override
			public Output apply(Input input) {
				if(!map.containsKey(input)) {
					// if need be, we call the outer one, but pass a handle to ourselves (which is has the cache)
					map.put(input, trueRecursive.apply(input, entryPoint[0]));
				}
				return map.get(input);
			}
			
		};
		
		return entryPoint[0];
	}
}

package ca.winograd.interviews.euler.p0;

import ca.winograd.interviews.euler.Primes;

public class Problem7 {
	
	public static void main(String [] args) {
		Primes primes = new Primes();
		System.out.println(primes.findPrime(10000));
	}
}

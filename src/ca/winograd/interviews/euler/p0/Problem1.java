package ca.winograd.interviews.euler.p0;

public class Problem1 {

	private static final int MAX = 10;
	
	public static void main(String [] args) {
		int sum = 0;
		for(int i = 0; i < MAX; i++) {
			if(i % 5 == 0 || i % 3 == 0) {
				sum += i;
			}
		}
		
		System.out.println("Sum is: " + sum);
	}
	
}

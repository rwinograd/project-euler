package ca.winograd.interviews.euler.p0;

public class Problem4 {

	public boolean isPalindrome(int num) {
		String str = String.valueOf(num);

		//12321
		//length = 5, length / 2 = 2
		//0,5-0-1=4
		//1,5-1-1=3
		//2,5-2-1=2
		for(int i = 0; i < str.length() / 2; i++) {
			int first = i;
			int last = str.length() - i - 1;
			
			if(str.charAt(first) != str.charAt(last)) {
				return false;
			}
		}
		
		return true;
		
	}
	
	private static final int MIN = 10;
	private static final int MAX = MIN*10;
	
	public static void main(String [] args) {
		Problem4 p = new Problem4();
		int max = 0;
		for(int i = MIN; i < MAX; i++) {
			for(int j = MIN; j < MAX; j++) {
				int test = i*j;
				if(p.isPalindrome(test) && test > max) {
					max = test;
				}
			}
		}
		
		System.out.println("Max:" + max);
	}
}

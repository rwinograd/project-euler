package ca.winograd.interviews.euler.p0;

import ca.winograd.interviews.euler.Function;
import ca.winograd.interviews.euler.Memoize;
import ca.winograd.interviews.euler.RecursiveFunction;

public class Problem2 {

	public final static int MAX = 4000000;
	
	private final Function<Integer, Integer> fibinnoci = Memoize.memoize(new RecursiveFunction<Integer, Integer>() {
		@Override
		public Integer apply(Integer input, Function<Integer, Integer> fn) {
			if(input == 0) return 1;
			if(input == 1) return 1;
			
			return fn.apply(input.intValue() - 1) + fn.apply(input.intValue() - 2);
		}
	});
	
	public static void main(String [] args) {
		Problem2 problem = new Problem2();
		
		int sum = 0;
		
		int i = 0;
		int fib = problem.fib(i);
		while(fib < MAX) {
			if(fib % 2 == 0) {
				sum += fib;
			}
			
			i++;
			fib = problem.fib(i);
		}
		
		System.out.println("Sum:" + sum);
	}

	public int fib(int i) {
		return fibinnoci.apply(i);
	}
	
}

package ca.winograd.interviews.euler.p0;

public class Problem6 {

	private final int max;
	
	public Problem6(int m) {
		max = m + 1;
	}
	
	public int squareOfSum() {
		int sum = 0;
		for(int i = 0; i < max; i++) {
			sum += i;
		}
		
		return sum*sum;
	}
	
	public int sumOfSquares() {
		int sum = 0;
		for(int i = 0; i < max; i++) {
			sum += (i*i);
		}
		
		return sum;		
	}
	
	public static void main(String [] args) {
		Problem6 proj = new Problem6(100);
		
		int squareOfSum = proj.squareOfSum();
		System.out.println("squareOfSum:" + squareOfSum);
		
		int sumOfSquares = proj.sumOfSquares();
		System.out.println("sumOfSquares:" + sumOfSquares);
		
		System.out.println("Diff:" + (squareOfSum-sumOfSquares));
	}
	
}

package ca.winograd.interviews.euler.p0;

import java.util.Set;

import ca.winograd.interviews.euler.Primes;
import ca.winograd.interviews.euler.Strings;

public class Problem3 {
	
	private static final Primes PRIMES = new Primes();
	
	public static void main(String [] args) {
		System.out.println("Factors: " + Strings.join(PRIMES.primeFactorize(600851475143L), ", "));
	}
}

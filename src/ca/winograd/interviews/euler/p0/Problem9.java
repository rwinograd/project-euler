package ca.winograd.interviews.euler.p0;

public class Problem9 {

	public static void main(String [] args) {
		//a+b+c=100
		//
		//a^2+b^2=c^2
		//c= sqrt(a^2+b^2)
		//a+b+sqrt(a^2+b^2)=1000
		
		for(int i = 1; i < 1000; i++) {
			for(int j = 1; j < 1000; j++) {
				double c = Math.sqrt(i*i+j*j);
				if(c - Math.floor(c) < 1E-6) {
					if(Math.abs(i+j+c- 1000) < 1E-6) {
						System.out.println("a: " + i + ",b: " + j + ",c: " + c);
						System.out.println("mult: " + i*j*(int)c);
					}	
				}
			}
		}
	}
}

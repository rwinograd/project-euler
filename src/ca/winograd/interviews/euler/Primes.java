package ca.winograd.interviews.euler;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class Primes {
	
	// Keeps a little cache of the primes that have been seen to make searching many times easier
	private int max;
	private final ArrayList<Integer> primes;

	public Primes() {
		max = 2;
		
		primes = new ArrayList<Integer>();
		primes.add(2);
	}
	
	public void seedPrimes(double candidate) {
		seedPrimes((long) candidate);
	}
	
	public void seedPrimes(long candidate) {
		if(candidate < max) {
			return;
		}
		for(int i = max + 1; i <= candidate; i++) {
			boolean isPrime = true;
			for(Integer p : primes) {
				if(i % p.intValue() == 0) { 
					isPrime = false;
				}
				
				if (p > Math.sqrt(i) || !isPrime) {
					break;
				}
			}
			
			if(isPrime) {
				primes.add(Integer.valueOf(i));
			}
		}
	}

	public int findPrime(int nth) {
		int i = primes.get(primes.size() - 1) + 1;
		while(primes.size() < nth) {
			boolean isPrime = true;
			for(Integer prime : primes) {
				if(i % prime.intValue() == 0) {
					isPrime = false;
					break;
				}
			}
			
			if(isPrime) {
				primes.add(Integer.valueOf(i));
			}
			i++;
		}
		
		max = primes.get(nth - 1);
		return max;
	}

	public int getNumberOfDivisors(long target) {
		seedPrimes(Math.sqrt(target));
		long num = target;
		int divisors = 1;
		
		for(Integer val : primes) {
			int count = 1;
			while(num % val == 0) {
				num = num / val;
				count++;
			}
				
			divisors *= count;
			if(num == 1) {
				break;
			}
		}
		return divisors;
	}

	public boolean isPrime(int candidate) {
		seedPrimes(candidate);
		
		return primes.contains(Integer.valueOf(candidate));
	}

	public Set<Integer> primeFactorize(long num) {
		double sqrt = Math.sqrt(num);
		seedPrimes(sqrt);
		
		Set<Integer> primeFactors = new TreeSet<Integer>();
		for(Integer p : primes) {
			while(num % p == 0) {
				primeFactors.add(p);
				num = num / p;
			}
			
			if(num == 1) {
				break;
			}
		}
		
		return primeFactors;
	}
}

package ca.winograd.interviews.euler;

import java.util.Iterator;

public final class Strings {

	public final static String join(Iterable<?> vals, String sep) {
		StringBuilder sb = new StringBuilder();
		
		Iterator<?> it = vals.iterator();
		if(it.hasNext()) {
			sb.append(it.next());
		}
		while(it.hasNext()) {
			sb.append(sep).append(it.next());
		}
		
		return sb.toString();
	}

	public static String join(Iterable<?> vals) {
		return join(vals, "");
	}
}

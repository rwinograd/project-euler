package ca.winograd.interviews.euler.p1;

import java.math.BigInteger;

public class Problem16 {

	private static final BigInteger TWO = new BigInteger("2");
	
	public static void main(String [] args) {
		BigInteger bi = new BigInteger("1");
		for(int i = 0; i < 1000; i++) {
			bi = bi.multiply(TWO);

			System.out.println("2^" + (i+1) + ": " + sumDigits(bi));
		}
	}

	private static long sumDigits(BigInteger bi) {
		long vals = 0;
		String s = bi.toString(10);
		for(int j = 0; j < s.length(); j++) {
			vals += Integer.valueOf(s.charAt(j)-48);
		}
		return vals;
	}
}

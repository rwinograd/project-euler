package ca.winograd.interviews.euler.p1;

public class Problem17 {

	private static final String [] ONES = {"one","two","three","four","five",
		"six","seven","eight","nine"	
	};
	
	private static final String [] TENS = {
		"ten", "twenty", "thirty", "forty", "fifty", "sixty",
		"seventy", "eighty", "ninety"
	};
	
	// 11 - 19 inclusive
	private static final String [] EXCEPTIONS = {
		"eleven", "twelve", "thirteen", "fourteen", "fifteen",
		"sixteen", "seventeen", "eighteen", "nineteen"
	};
	
	public static String toString(int remainder) {
		StringBuilder sb = new StringBuilder();
		
		// pull the thousands number off
		int thousands = (remainder / 1000);
		if(thousands > 0) {
			sb.append(ONES[thousands - 1]).append(" thousand");
		}
		remainder -= (thousands*1000);
		
		// pull the hundreds off
		int hundreds = (remainder / 100);
		if(hundreds > 0) {
			sb.append(ONES[hundreds - 1]).append(" hundred");
		}
		remainder -= (hundreds*100);
		
		// add the "and" if we have trailing characters
		if(remainder > 0 && (hundreds > 0 || thousands > 0)) {
			sb.append(" and");
		}
		
		// pull the tens and ones off
		if(remainder > 10 && remainder < 20) {
			sb.append(" ").append(EXCEPTIONS[remainder - 11]);
		} else {
			int tens = (remainder / 10);
			if(tens > 0) {
				sb.append(" ").append(TENS[tens - 1]);	
			}
			remainder -= (tens*10);
			
			int ones = (remainder);
			if(ones > 0) {
				sb.append(" ").append(ONES[ones - 1]);	
			}
		}
		
		return sb.toString();
	}
	
	public static int count(String val) {
		return val.replace(" ", "").length();
	}
	
	public static void main(String [] args) {
		int sum = 0;
		for(int i = 1; i <= 1000; i++) {
			sum += count(toString(i));
		}

		System.out.println("SUM:" + sum);
	}
	
}

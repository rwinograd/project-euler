package ca.winograd.interviews.euler.p1;

import ca.winograd.interviews.euler.Primes;

public class Problem10 {

	public final static Primes PRIMES = new Primes();
	
	private static final int MAX = 2000000;
	
	public static void main(String [] args) {		
		long sum = 2;
		for(int i = 3; i < MAX; i+=2) {
			if(PRIMES.isPrime(i)) {
				sum += i;
			}
		}
		
		System.out.println("Prime Sum: " + sum);
	}
	
}

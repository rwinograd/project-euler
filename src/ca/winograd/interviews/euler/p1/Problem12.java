package ca.winograd.interviews.euler.p1;

import ca.winograd.interviews.euler.Primes;

public class Problem12 {
	
	private final Primes PRIMES = new Primes();
	
	public long findNumberWithDivisors(int numDivisors) {
		long triangle = 0;
		long maxDivisors = 0;
		for(int i = 1; true; i++) {
			triangle += i;
			
			int divisors = PRIMES.getNumberOfDivisors(triangle);
			if(divisors > numDivisors) { 
				return triangle;
			} 
			
			if(divisors > maxDivisors) {
				maxDivisors = divisors;
				System.out.println("Found " + maxDivisors + ", at:" + triangle);
			}
		}
	}
	
	public static void main(String [] args) {
		Problem12 prob = new Problem12();

		System.out.println("Number with divisors: " + prob.findNumberWithDivisors(5));
		System.out.println("Number with divisors: " + prob.findNumberWithDivisors(500));
	}
	
}

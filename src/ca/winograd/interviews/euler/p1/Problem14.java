package ca.winograd.interviews.euler.p1;

import java.util.LinkedList;
import java.util.List;

public class Problem14 {	
	public final int [] memoize;
	
	public Problem14(int max) {
		memoize = new int[max * 4];
		memoize[1] = 1;
	}
	
	// Recursive method is too deep, so we do it iteratively
	public int collatz(long val) {
		if(memoize[(int)val] != 0) return memoize[1];
		
		List<Long> stack = new LinkedList<Long>();
		
		while(val >= memoize.length || memoize[(int)val] == 0) {
			stack.add(val);
			val = ((val % 2 == 0) ? val/2 : (3*val + 1) );
		}
		
		int i = 0;
		for(Long s : stack) {
			if(s < memoize.length) {
				// don't bother saving this one since we have to be somewhat conservative with memory
				// (note: consider having an LRU cache here. All the int casts are safe since we just throw out big values and we know
				// that our final answer has to be under 1,000,000)
				memoize[s.intValue()] = stack.size() + memoize[(int) val] - i;
			}
			i++;
		}
		
		return memoize[stack.get(0).intValue()];
	}
	
	private static final int MAX = 1000000;
	
	public static void main(String [] args) {
		Problem14 problem14 = new Problem14(MAX);
		
		int max = 0;
		for(int i = 1; i < MAX; i++) {
			int val = problem14.collatz(i);
			if(val > max) {
				max = val;
				System.out.println("Max: " + max + " @ " + i);
			}
		}
	}
	
}

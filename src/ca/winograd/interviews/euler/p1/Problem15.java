package ca.winograd.interviews.euler.p1;

public class Problem15 {
	
	private final int finalX;
	private final int finalY;
	
	public Problem15(int finalPos) {
		finalX = finalPos;
		finalY = finalPos;
	}
	
	public long getWaysToEnd(int currX, int currY) {
		long totalWays = 0;
		if(currX == finalX && currY == finalY) {
			return 1;
		}
		if(currX != finalX) {
			totalWays += (getWaysToEnd(currX + 1, currY));
		}
		if(currY != finalY) {
			totalWays += (getWaysToEnd(currX, currY + 1));
		}
		return totalWays;
	}
	
	public static void main(String [] args) {

		// Use the full algorithm on the first 10
		long last = 6;
		for(int i = 1; i < 10; i++) {
			final Problem15 prob15 = new Problem15(i+2);
			long curr = prob15.getWaysToEnd(0, 0);
			System.out.println(String.format("Ways to end for %dx%d: %d", i+2,i+2, curr));
			
			// Look a pattern!
			long div = (((i+2) * curr / last));
			System.out.println("Div/2 for i: " + div/2);
			
			last = curr;
		}
		
		// Use the pattern for everything else!
		long count = 6;
		for(int i = 1; i < 19; i++) {
			count *= (3*(i+2) + i);
			count /= (i+2);
			
			System.out.println(String.format("%dx%d: %d", i+2,i+2, count));
		}
	}
	public static long round(double a) {
	    if (a != 0x1.fffffffffffffp-2) // greatest double value less than 0.5
	        return (long)Math.floor(a + 0.5d);
	    else
	        return 0;
	}
	
}

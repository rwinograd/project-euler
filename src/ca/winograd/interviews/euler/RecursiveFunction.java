package ca.winograd.interviews.euler;

/**
 * Allows a memoizing call to be passed into the function 
 *
 * @param <Input>
 * @param <Output>
 */
public interface RecursiveFunction<Input, Output> {
	public Output apply(Input input, Function<Input, Output> fn);
}

package ca.winograd.interviews.euler;

/**
 * Normal interface for a functor
 *
 * @param <Input>
 * @param <Output>
 */
public interface Function<Input, Output> {
	public Output apply(Input input);
}
